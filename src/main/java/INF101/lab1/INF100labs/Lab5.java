package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
       ArrayList<Integer> result = new ArrayList<>();
       for (Integer i : list){
         result.add(i * 2);
       }
       return result;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
       ArrayList<Integer> result = new ArrayList<>();
       for (int i :list){
         if (i != 3) {
            result.add(i);
         }
       }
       return result;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
      ArrayList<Integer> result = new ArrayList<>();
      for (int i : list)   {
         if (!(result.contains(i))){
            result.add(i);
         }
      }
      return result;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
       for (int i=0; i<a.size(); i++){
         a.set(i, a.get(i) + b.get(i));
       }
    }

}