package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        //radene
        int rowSum = sumRow(grid, 0);
        for (int i=1; i<grid.size(); i++){
            if (sumRow(grid, i) != rowSum) {
                return false;
            }
        }
        //kolonnene
        int colSum = sumCol(grid, 0);
        for (int col=1; col<grid.get(0).size(); col++) {
            if (sumCol(grid, col) != colSum){
                return false;
            }
        }
        return true;
        }

        public static int sumRow(ArrayList<ArrayList<Integer>> grid, int row) {
            int result = 0;
            for (int i = 0; i < grid.get(row).size(); i++) {
                result += grid.get(row).get(i);
            }
            return result;
        }
    
        public static int sumCol(ArrayList<ArrayList<Integer>> grid, int col) {
            int result = 0;
            for (int i = 0; i < grid.size(); i++) {
                result += grid.get(i).get(col);
            }
            return result;



    }

    }
