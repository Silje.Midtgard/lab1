package INF101.lab1.INF100labs;

import java.util.Arrays;
import java.util.Collections;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void findLongestWords(String word1, String word2, String word3) {

        int word1Lenght = word1.length();
        int word2Lenght = word2.length();
        int word3Lenght = word3.length();
        int longest = Collections.max(Arrays.asList(word1Lenght, word2Lenght, word3Lenght));
        
        if(word1Lenght == longest){
            System.out.println(word1);
        }
        if(word2Lenght == longest){
            System.out.println(word2);
        }
        if(word3Lenght == longest){
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if(year % 4 == 0){

            if(year % 400 == 0){
                return true;
            }
            
            if( year % 100 == 0){
                return false;
        }
        return true;
           
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if(num >0){
            if(num % 2 == 0){
                return true;
            }
        }
        return false;
    }
}
